package com.bulletinboard;

public class User {
    private int idUser;
    private String aliasUser;
    private String password;
    private String mobileNum;
    private String email;

    public int getIdUser() {
        return idUser;
    }
    
    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
    
    public String getAliasUser() {
        return aliasUser;
    }
    
    public void setAliasUser(String aliasUser) {
        this.aliasUser = aliasUser;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }
    
    public String getMobileNum() {
        return mobileNum;
    }
    
    public void setMobileNum(String mobileNum) {
        this.mobileNum = mobileNum;
    }
    
    public String getEmail() {
        return email;
    }
    
    public void setEmail(String email) {
        this.email = email;
    }

    public User(int idUser, String aliasUser, String password, String mobileNum, String email) {
        this.idUser = idUser;
        this.aliasUser = aliasUser;
        this.password = password;
        this.mobileNum = mobileNum;
        this.email = email;
    }
    
    public User(int idUser,String password, String email) {
        this.idUser = idUser;
        this.password = password;
        this.email = email;
    }

    public User() {
    }
}