package com.bulletinboard;
import java.sql.*;
import java.util.ArrayList;

public class Manager {

    private Connection conn;
    private String url = "jdbc:mysql://localhost/bulletin_board?serverTimezone=UTC";
    private String nom = "bulletin_board";
    private String pas = "bulletin_board";

    public boolean openConexion() throws SQLException{
        try {

            conn = DriverManager.getConnection(url,nom,pas);
            if(conn.isValid(0)){
                return true;
            }
            else{
            return false;
            }
        }catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }   

    public void closeConexion() {
        try {
            if(conn != null && !conn.isClosed()){
                conn.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
    }

    public boolean createUser(User u) throws SQLException{

        try {
            if(openConexion()){
                
                PreparedStatement consulta = conn.prepareStatement("INSERT INTO bulletin_board.user (alias_user, password, mobile_num, email) values (?,?,?,?)");
                consulta.setString(1, u.getAliasUser());
                consulta.setString(2, u.getPassword());
                consulta.setString(3, u.getMobileNum());
                consulta.setString(4, u.getEmail());
                
                consulta.executeUpdate();
                
                closeConexion();
                
                return true;
            }
            else{
                return false;
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }

    }

    public UserLogIn loginUser(UserLogIn ul) {

        UserLogIn usl = new UserLogIn();
        try {
            if(openConexion()){
                
                PreparedStatement st = conn.prepareStatement("SELECT id_user, ?, ? FROM bulletin_board.user;");
                st.setString(1, ul.getAlias());
                st.setString(2, ul.getPassword());
                ResultSet rs = st.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt(1);
                    String name = rs.getString(2);
                    String pass = rs.getString(3);

                    usl = new UserLogIn(id,name,pass);
                    
                }
                closeConexion();
            }
           
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return usl;
    }

    public void UserChangeDetails(User us){

        try {
            openConexion();;
            PreparedStatement st = conn.prepareStatement("UPDATE bulletin_board.user/n"+
                                            "SET password = ?, email = ? WHERE id_user = ?");
            st.setString(1, us.getPassword());
            st.setString(2, us.getEmail());
            st.setInt(3, us.getIdUser());
            st.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public User UserDetails(int id_user){

        User ud = new User();
        try {
            openConexion();
            PreparedStatement st = conn.prepareStatement("SELECT * FROM bulletin_board.user where id_user = ?;");
            st.setInt(1, id_user);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String alias = rs.getString(2);
                String pass = rs.getString(3);
                String mobile = rs.getString(4);
                String email = rs.getString(5);

                ud = new User(id, alias, pass, mobile, email);              
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return ud;
    }

    public void createPost(Post post) {

        try {

            openConexion();

            PreparedStatement consulta = conn.prepareStatement(

                    "INSERT INTO bulletin_board.post (title, content, image, phone, email, location, latitude, longitude, creation_date, view, is_anon, user_id_user, section_id_section) values (?,?,?,?,?,?,?,?,?,?,?,?,?)");

            consulta.setString(1, post.getTitle());
            consulta.setString(2, post.getContent());
            consulta.setString(3, post.getImage());
            consulta.setString(4, post.getPhone());
            consulta.setString(5, post.getEmail());
            consulta.setString(6, post.getLocation());
            consulta.setDouble(7, post.getLatitude());
            consulta.setDouble(8, post.getLongitude());
            consulta.setTimestamp(9, post.getCreationDate());
            consulta.setInt(10, post.getView());
            consulta.setByte(11, post.getIsAnon());
            consulta.setInt(12, post.getUserId());
            consulta.setInt(13, post.getSectionId());

            consulta.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    public GetDetailPost postListDetails(int id_post) {

        GetDetailPost post = new GetDetailPost();
        try {
            openConexion();
            PreparedStatement st = conn.prepareStatement("SELECT * FROM bulletin_board.post WHERE id_post = ?;");
            st.setInt(1, id_post);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String content = rs.getString(3);
                String image = rs.getString(4);
                String phone = rs.getString(5);
                String email = rs.getString(6);
                String location = rs.getString(7);
                java.sql.Timestamp creationDate= rs.getTimestamp(10);
                int view= rs.getInt(11);
                byte isAnon = rs.getByte(12);
                int userId = rs.getInt(13);
                int sectionId = rs.getInt(14);
                
                post = new GetDetailPost(id, title, content, image, phone, email, location, creationDate, view, isAnon, userId, sectionId);

               
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return post;
    }

    public ArrayList<PostLisT> postlist(int id_section){
        ArrayList<PostLisT> list = new ArrayList<PostLisT>();
        try {
            openConexion();
            PreparedStatement st = conn.prepareStatement("SELECT id_post, title, image FROM bulletin_board.post where section_id_section = ?;");
            st.setInt(1, id_section);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String image = rs.getString(3);

                PostLisT p = new PostLisT(id, title, image);
                list.add(p);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<Post> postPorUser(int id_user){

        ArrayList<Post> list = new ArrayList<Post>();
        try {
            openConexion();
            PreparedStatement ps = conn.prepareStatement("SELECT id_post, title, content, image, phone, email, location, creation_date, view, section_id_section FROM bulletin_board.post WHERE user_id_user = ?;");
            ps.setInt(1, id_user);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String content = rs.getString(3);
                String image = rs.getString(4);
                String phone = rs.getString(5);
                String email = rs.getString(6);
                String location = rs.getString(7);
                java.sql.Timestamp creationDate= rs.getTimestamp(8);
                int view= rs.getInt(9);
                int sectionId = rs.getInt(10);

                Post post = new Post(id, title, content, image, phone, email, location,creationDate, view, sectionId);
                list.add(post);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public void deletePost(int id_post){

        try {
            openConexion();
            PreparedStatement st = conn.prepareStatement("DELETE FROM bulletin_board.post WHERE id_post = ?;");
            st.setInt(1, id_post);
            st.executeQuery();

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public ArrayList<Section> sectionList() {

        ArrayList<Section> list = new ArrayList<Section>();
        try {
            openConexion();
            String sql = "Select * FROM section";
            Statement st = conn.createStatement();
            ResultSet rs = st.executeQuery(sql);
            while (rs.next()) {
                int id = rs.getInt("id_section");
                String name = rs.getString("section_name");

                Section se = new Section(id,name);
                list.add(se);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public ArrayList<PostLisT> buscarPostUbicacion(Double Ulat, Double Ulong, int distance, int section, String commonWord ){

        ArrayList<PostLisT> list = new ArrayList<PostLisT>();
        try {
            openConexion();
            PreparedStatement ps = conn.prepareStatement("SELECT id_post, title, image,  ("+
                "6378.137 * acos(cos(radians(?)) * cos(radians(latitude)) * cos(radians(longitude) - radians(?)) + sin(radians(?)) * sin(radians(latitude)))) as distancia "+
            "FROM post "+
            "WHERE "+
                "section_id_section = ? and content LIKE ? and "+
                "latitude between (-100) and (100) and "+
                "longitude between (-100) and (100) "+ 
            "HAVING distancia < ?;");
            ps.setDouble(1, Ulat);
            ps.setDouble(2, Ulong);
            ps.setDouble(3, Ulat);
            ps.setInt(4, section);
            ps.setString(5, "%" + commonWord + "%");
            ps.setInt(6, distance);

            ResultSet rs = ps.executeQuery();
            while(rs.next()){
                int id = rs.getInt(1);
                String title = rs.getString(2);
                String image = rs.getString(3);

                PostLisT post = new PostLisT(id, title, image);

                list.add(post);
            }
        } catch  (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

}