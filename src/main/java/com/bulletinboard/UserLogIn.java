package com.bulletinboard;

public class UserLogIn {
    private int id;
    private String alias;
    private String password;

    public String getAlias() {
        return alias;
    }
    
    public void setAlias(String alias) {
        this.alias = alias;
    }
    
    public String getPassword() {
        return password;
    }
    
    public void setPassword(String password) {
        this.password = password;
    }

    public UserLogIn(String alias, String password) {
        this.alias = alias;
        this.password = password;
    }

    public UserLogIn(int id, String alias, String password) {
        this.id = id;
        this.alias = alias;
        this.password = password;
    }

    public UserLogIn() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    
    

    
}