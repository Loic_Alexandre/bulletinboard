package com.bulletinboard.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bulletinboard.Manager;
import com.bulletinboard.User;
import com.google.gson.Gson;

@WebServlet(name = "UserDetails", urlPatterns = {"/servlets/UserDetails"})
public class UserDetails extends HttpServlet {

    private Gson gson = new Gson();
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                
            HttpSession sesion = request.getSession();
            Manager manager = new Manager();
            User user = new User();
            int id = (int) sesion.getAttribute("usuario");
            user = manager.UserDetails(id);

            String UserDetailJsonString = this.gson.toJson(user);
            
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(UserDetailJsonString);
            out.flush();
    }

    @Override 
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
    throws ServletException, IOException {

        Manager manager = new Manager();

        int id_post = Integer.parseInt(request.getParameter("id_post"));

        manager.deletePost(id_post);

    }
}
