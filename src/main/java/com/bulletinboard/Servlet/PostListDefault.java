package com.bulletinboard.Servlet;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

import com.bulletinboard.Manager;
import com.bulletinboard.PostLisT;
import com.google.gson.Gson;


@WebServlet(name = "PostListDefault", urlPatterns = {"/servlets/PostListDefault"})
public class PostListDefault extends HttpServlet{
    
    private Gson gson = new Gson();
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

            Manager manager = new Manager();

            //Lista para los post del index solo muestra las columnas par previsualizar (titulo e imagen),
            // llama a los post dependiendo del ID de section 
            int id = 1; // id de section
            ArrayList<PostLisT> listPost = manager.postlist(id);
            String listPostJsonString = this.gson.toJson(listPost);
            
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(listPostJsonString);
            out.flush();
        }
};
