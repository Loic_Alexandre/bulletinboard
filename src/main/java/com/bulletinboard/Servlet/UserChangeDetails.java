package com.bulletinboard.Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.bulletinboard.Manager;
import com.bulletinboard.User;

@WebServlet(name = "UserChangeDetails", urlPatterns = {"/servlets/UserChangeDetails"})
public class UserChangeDetails  extends HttpServlet{
    
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }
    @Override
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        HttpSession sesion = request.getSession();
        Manager manager = new Manager();
        try {
            
            int id = (int) sesion.getAttribute("usuario");
            String pass = request.getParameter("confirmNewPassword");
            String mail = request.getParameter("confirmNewEmail");
            User UCD = new User(id,pass,mail);
            manager.UserChangeDetails(UCD);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
