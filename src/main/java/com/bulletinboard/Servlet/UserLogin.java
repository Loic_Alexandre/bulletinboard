package com.bulletinboard.Servlet;
import com.bulletinboard.Manager;
import com.bulletinboard.UserLogIn;
import java.io.*;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name = "UserLogin", urlPatterns = {"/servlets/UserLogin"})
public class UserLogin extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    //me llega la url "proyecto/login/out"
    String action=(request.getPathInfo()!=null?request.getPathInfo():"");
    HttpSession sesion = request.getSession();
    if(action.equals("/out")){
        sesion.invalidate();
        response.sendRedirect("http://localhost:8080/bulletin_board/");
    }else{
       
    }
}
    
    public void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        Manager manager = new Manager();
        UserLogIn UL = new UserLogIn();

        HttpSession sesion = request.getSession();
        String usu, pass;
        usu = request.getParameter("userAlias");
        pass = request.getParameter("userPassword");
        
        UserLogIn ul = new UserLogIn(usu,pass);
        
        UL = manager.loginUser(ul);
        if(usu.equals(UL.getAlias()) && pass.equals(UL.getPassword())
         && sesion.getAttribute("usuario") == null){
            sesion.setAttribute("usuario", UL.getId());
            sesion.setAttribute("alias", UL.getAlias());
            String alias = UL.getAlias();
            int id = UL.getId();
            String idString = String.valueOf(id);
            Cookie userCookie = new Cookie("alias",alias);
            Cookie idCookie = new Cookie("id", idString);
            userCookie.setMaxAge(-2);
            idCookie.setMaxAge(-2);
            response.addCookie(userCookie);
            response.addCookie(idCookie);
            System.out.println(sesion.getAttribute("usuario"));
            response.sendRedirect("http://localhost:8080/bulletin_board/");
            
        }
        else{
            PrintWriter out = response.getWriter();
                out.print("Se ha encontrado un problema al iniciar secion");
        }

    }   
    
}; 
