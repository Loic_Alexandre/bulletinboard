package com.bulletinboard.Servlet;
import com.bulletinboard.Manager;
import com.bulletinboard.User;
import java.io.*;
import java.sql.SQLException;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name = "UserRegister", urlPatterns = {"/servlets/UserRegister"})
public class UserRegister extends HttpServlet {
    
    private static final long serialVersionUID = 1L;

    @Override
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
                // PrintWriter out = response.getWriter();
                // out.print("hola Gabriel");

            RequestDispatcher dispatcher = request.getRequestDispatcher("../../../../webapp/components/modal/modal.jsp");
            dispatcher.forward(request, response);
    }

    
    public void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Manager manager = new Manager();
        
        String alias = request.getParameter("userAlias");
        String pass = request.getParameter("userPassword");
        String mobile = request.getParameter("numMobile");
        String email = request.getParameter("userEmail");
        User user = new User(1, alias, pass, mobile, email);
        PrintWriter out = response.getWriter();
                out.print(alias+pass+mobile+email);
        try {
            if(manager.createUser(user)){

            }
            else{
                out.print("ha habido un error");
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        response.sendRedirect("http://localhost:8080/bulletin_board/"); //bulletin_board?successful=true
        // programar el index para que cuando le llege un successful true muentre un cartel de usuario exitoso; 

        // RequestDispatcher dispatcher = request.getRequestDispatcher("../index.jsp");
        //      dispatcher.forward(request, response);
     }   
    
}; 