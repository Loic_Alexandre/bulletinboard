package com.bulletinboard.Servlet;
import com.bulletinboard.Manager;
import com.bulletinboard.Post;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;

import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@WebServlet(name = "PostListUser", urlPatterns = {"/servlets/PostListUser"})
public class PostListUser extends HttpServlet {

    private Gson gson = new Gson();
    private static final long serialVersionUID = 1L;

 @Override 
 public void doGet(HttpServletRequest request, HttpServletResponse response) 
     throws ServletException, IOException {

        //lista los post del usuario enviando el id del usuario tomado de la sesion iniciada, 
        // lista toda la informacion del post que luego front manejará que mostrar y que no

        HttpSession sesion = request.getSession();
        Manager manager = new Manager();
        int id_user = (int) sesion.getAttribute("usuario");
        ArrayList<Post> listPostUser = manager.postPorUser(id_user);
        String listPostUserJsonString = this.gson.toJson(listPostUser);
            
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(listPostUserJsonString);
            out.flush();
     }
    
    @Override 
    public void doPost(HttpServletRequest request, HttpServletResponse response) 
        throws ServletException, IOException {

            //borra el post desde la pestaña de los detalles de usuario donde estan todos sus post,
            // al seleccionar el post que desea eliminar tomamos su id_post y lo pasamos como parametro
            Manager manager = new Manager();

            int id_post = Integer.parseInt(request.getParameter("id_post"));

            manager.deletePost(id_post);

    }
    
};