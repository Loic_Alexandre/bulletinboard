package com.bulletinboard.Servlet;
import com.bulletinboard.Manager;
import com.bulletinboard.Section;
import com.google.gson.Gson;
import com.bulletinboard.Post;
import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;



@WebServlet(name = "NewPost", urlPatterns = {"/servlets/NewPost"})
public class NewPost extends HttpServlet {
    
    private Gson gson = new Gson();
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Manager manager = new Manager();
        
        try {
            // manda al post form una lista con las seciones para usarles en un select
            ArrayList<Section> listS = manager.sectionList();
            String listSectionJsonString = gson.toJson(listS);
            
            PrintWriter out = response.getWriter();
            response.setContentType("application/json");
            response.setCharacterEncoding("UTF-8");
            out.print(listSectionJsonString);
            out.flush();

            // request.setAttribute("listSection", listS);
            // RequestDispatcher rd = request.getRequestDispatcher("../components/postForm.jsp");
            // rd.forward(request, response);

        } catch (Exception e) {
            e.printStackTrace();
            throw new ServletException(e);
        }

    }   
    
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {

        //crea un nuevo post tomado los parametros entre "" desde el postForm.jsp
        
        HttpSession sesion = request.getSession();
        Manager manager = new Manager();
        
        String title = request.getParameter("titlePost");
        String content = request.getParameter("contentPost");
        String image = request.getParameter("imagePostFake");
        String phone = request.getParameter("phonePost");
        String email = request.getParameter("emailPost");
        String location = request.getParameter("locationPost");
        Double lat = Double.parseDouble(request.getParameter("latitud"));
        Double lon = Double.parseDouble(request.getParameter("longitud"));
        java.sql.Timestamp creationDate = new java.sql.Timestamp(System.currentTimeMillis());
        byte anon = 0;
        int view = 0;
        int user = (int) sesion.getAttribute("usuario");
        int section = Integer.parseInt(request.getParameter("idSection"));

         Post post = new Post(1, title, content, image, phone, email, location, lat, lon, creationDate, view, anon, user, section);
        System.out.println(post);
         manager.createPost(post);

         response.sendRedirect("http://localhost:8080/bulletin_board/");

    }   
    
}; 