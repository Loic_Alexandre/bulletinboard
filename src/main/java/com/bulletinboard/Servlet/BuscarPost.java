package com.bulletinboard.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bulletinboard.Manager;
import com.bulletinboard.PostLisT;
import com.google.gson.Gson;

@MultipartConfig
@WebServlet(name = "BuscarPost", urlPatterns = {"/servlets/BuscarPost"})
public class BuscarPost extends HttpServlet {

    private Gson gson = new Gson();
    private static final long serialVersionUID = 1L;

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

                System.out.println("Dentro de BuscarPost");

        // aqui se capturan las variables para hacer la busquda por ubicacion y que que
        // buscas

        Manager manager = new Manager();
        ArrayList<PostLisT> listPost = new ArrayList<PostLisT>();

        Double lat = Double.parseDouble(request.getParameter("latitud"));
        Double lon = Double.parseDouble(request.getParameter("longitud"));
        int section = Integer.parseInt(request.getParameter("searchSection"));
        String like = request.getParameter("searchContent");
        int distance = Integer.parseInt(request.getParameter("searchDistance"));

        listPost = manager.buscarPostUbicacion(lat, lon, section, distance, like);

        String SearchPostJsonString = this.gson.toJson(listPost);
        
        PrintWriter out = response.getWriter();
        response.setContentType("application/json");
        response.setCharacterEncoding("UTF-8");
        out.print(SearchPostJsonString);
        out.flush();

    }
}
