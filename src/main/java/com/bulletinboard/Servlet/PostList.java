package com.bulletinboard.Servlet;

import com.bulletinboard.GetDetailPost;
import com.bulletinboard.Manager;
import com.bulletinboard.PostLisT;
import com.google.gson.Gson;

import java.io.*;
import java.util.ArrayList;
import javax.servlet.*;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;

@MultipartConfig
@WebServlet(name = "PostList", urlPatterns = { "/servlets/PostList" })
public class PostList extends HttpServlet {

        private Gson gson = new Gson();
        private static final long serialVersionUID = 1L;

        @Override
        public void doGet(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException {

                Manager manager = new Manager();

                // Lista para los post del index solo muestra las columnas par previsualizar
                // (titulo e imagen),
                // llama a los post dependiendo del ID de section
                // int id = 0; // id de section
                ArrayList<PostLisT> listPost = manager.postlist(1);
                String listPostJsonString = this.gson.toJson(listPost);

                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                out.print(listPostJsonString);
                out.flush();

                // request.setAttribute("listPost", listPost);

                // RequestDispatcher dispatcher =
                // request.getRequestDispatcher("../../../../webapp/components/.jsp");
                // dispatcher.forward(request, response);
        }

        public void doPost(HttpServletRequest request, HttpServletResponse response)
                        throws ServletException, IOException {

                // envia una peticion para ver el post selectionado del index con su detalle,
                // pasando el id del post

                Manager manager = new Manager();
                GetDetailPost ps = new GetDetailPost();
                String data = request.getParameter("id");
                int id = Integer.parseInt(data);
                ps = manager.postListDetails(id);

                String postJsonString = this.gson.toJson(ps);

                PrintWriter out = response.getWriter();
                response.setContentType("application/json");
                response.setCharacterEncoding("UTF-8");
                out.print(postJsonString);
                // out.print(ps);
                out.flush();
        }
}
