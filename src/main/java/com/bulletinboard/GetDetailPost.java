package com.bulletinboard;

import java.sql.Timestamp;


public class GetDetailPost{
    private int idPost;
    private String title;
    private String content;
    private String image;
    private String phone;
    private String email;
    private String location;
    private Double latitude;
    private Double longitude;
    private java.sql.Timestamp creationDate;
    private byte isAnon;
    private int view;
    private int userId;
    private int sectionId;

    public int getIdPost() {
        return idPost;
    }

    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public java.sql.Timestamp getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(java.sql.Timestamp creationDate) {
        this.creationDate = creationDate;
    }

    public byte getIsAnon() {
        return isAnon;
    }

    public void setIsAnon(byte isAnon) {
        this.isAnon = isAnon;
    }

    public int getView() {
        return view;
    }

    public void setView(int view) {
        this.view = view;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public int getSectionId() {
        return sectionId;
    }

    public void setSectionId(int sectionId) {
        this.sectionId = sectionId;
    }


    public GetDetailPost(int idPost, String title, String content, String image, String phone, String email, String location,
            Timestamp creationDate, int view, byte isAnon, int userId, int sectionId) {
        this.idPost = idPost;
        this.title = title;
        this.content = content;
        this.image = image;
        this.phone = phone;
        this.email = email;
        this.location = location;
        this.creationDate = creationDate;
        this.view = view;
        this.isAnon = isAnon;
        this.userId = userId;
        this.sectionId = sectionId;
    }

    public GetDetailPost(){
        
    }
}

