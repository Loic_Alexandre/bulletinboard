package com.bulletinboard;

public class Section{
    private int idSection;
    private String sectionName;

    public int getIdSection() {
        return idSection;
    }
    public void setIdSection(int idSection) {
        this.idSection = idSection;
    }
    public String getSectionName() {
        return sectionName;
    }
    public void setSectionName(String sectionName) {
        this.sectionName = sectionName;
    }
    public Section(int idSection, String sectionName) {
        this.idSection = idSection;
        this.sectionName = sectionName;
    }
    public Section(){

    }
    
}