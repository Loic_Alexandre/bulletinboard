package com.bulletinboard;

public class PostLisT {

    private int idPost;
    private String title;
    private String image;
    public int getIdPost() {
        return idPost;
    }
    public void setIdPost(int idPost) {
        this.idPost = idPost;
    }
    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }
    public String getImage() {
        return image;
    }
    public void setImage(String image) {
        this.image = image;
    }
    public PostLisT(int idPost, String title, String image) {
        this.idPost = idPost;
        this.title = title;
        this.image = image;
    }
    public PostLisT() {
    }
    
}
