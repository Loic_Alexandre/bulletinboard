$(document).ready(function () {

    // const postCardsInfo = [{
    //     id:"1",
    //     title: "Ofrezco trabajos de fontanería blablablablablablablablablablablablablablablablablablablablablabla",
    //     description:"Discreción máxima, desatascamos todas las tuberías"
    // },
    // {
    //     id:"2",
    //     title: "Se busca niñera",
    //     description: "Necesito que alguien se ocupe de mis hijos mientras me fugo un fin de semana"
    // },
    // {
    //     id:"3",
    //     title: "Title 3",
    //     description: "Description 3"
    // },
    // {
    //     id:"4",
    //     title: "Title 4",
    //     description: "Description 4"
    // }];




    function retrieveDefaultList(){

        fetch('http://localhost:8080/bulletin_board/servlets/PostListDefault').then(function (arraySectionsJSON) {
            return arraySectionsJSON.json();
        }).then(function (arraySectionsParsed) {
            createList(arraySectionsParsed)
        });
    }

    retrieveDefaultList();



    function createList(defaultList){
        
        document.getElementById("1").innerHTML = "";
        for(let i = 0; i<defaultList.length; i++){
            const postCard = `
                         <li id="${defaultList[i].idPost}"class="postCardContainer">
                            <a style="text-decoration:none;color:black;" href='./pages/detailPostCard/detailPostCard.jsp?id=${defaultList[i].idPost}'>
                                <div class="postCard">
                                <div class="postCardTitleContainer">
                                    <p class="postCardTitle">${defaultList[i].title}</p>
                                </div>
                                <img class="postCardImg" id="logo" src="img/placeholder${Math.floor(Math.random()*5+1)}.png" alt="logo">
                                <div class="postCardOverlay"/>
                                </div>
                                </a>
                        </li>`;

            document.getElementById("1").innerHTML += postCard;
        }
    }


    $('#searchButton').on('click', function(){
        let searchTitle = $('#searchTitle').val()
        console.log(searchTitle);
        console.log(typeof(searchTitle));

        let inputLatitud = $('#inputLatitud').val()
        console.log(inputLatitud);
        console.log(typeof(inputLatitud));

        let inputLongitud = $('#inputLongitud').val()
        console.log(inputLongitud);
        console.log(typeof(inputLongitud));

        let searchDistance = $('#searchDistance').val()
        console.log(searchDistance);
        console.log(typeof(searchDistance));

        let inputSection = $('#inputSection').val()
        console.log(inputSection);
        console.log(typeof(inputSection));


        loadSearchList(searchTitle,inputLatitud,inputLongitud,searchDistance,inputSection)

    })

    $('#searchButtonDesktop').on('click', function(){
        let searchTitle = $('#searchTitleDesktop').val()
        console.log(searchTitle);
        console.log(typeof(searchTitle));

        let inputLatitud = $('#inputLatitudDesktop').val()
        console.log(inputLatitud);
        console.log(typeof(inputLatitud));

        let inputLongitud = $('#inputLongitudDesktop').val()
        console.log(inputLongitud);
        console.log(typeof(inputLongitud));

        let searchDistance = $('#searchDistanceDesktop').val()
        console.log(searchDistance);
        console.log(typeof(searchDistance));

        let inputSection = $('#inputSectionDesktop').val()
        console.log(inputSection);
        console.log(typeof(inputSection));


        loadSearchList(searchTitle,inputLatitud,inputLongitud,searchDistance,inputSection)

    })




    function loadSearchList(searchTitle,inputLatitud,inputLongitud,searchDistance,inputSection) {

        let formData = new FormData();
        formData.append("latitud", inputLatitud);
        formData.append("longitud", inputLongitud);
        formData.append("searchSection", inputSection);
        formData.append("searchContent", searchTitle);
        formData.append("searchDistance", searchDistance);

        fetch('http://localhost:8080/bulletin_board/servlets/BuscarPost', {
            method: 'POST',
            body: formData
        }).
            then(function (response) {
                console.log(response);
                return response.json();
            }).then(function (searchLisResult) {
                createList(searchLisResult);
                

            });

    }


})