<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cambiar los detalles del usuario</title>
    <script src="https://kit.fontawesome.com/82342f99b0.js" crossorigin="anonymous"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
    <script type="text/javascript" src="scriptFormChangeDetails.js"></script>
    <link rel="stylesheet" type="text/css" href="userChangeDetails.css">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.typekit.net/ifd2oig.css">
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
    <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
</head>
<body> 
    <div class="containerPost">
        <h3 class="cambiarContrasena">Cambiar Contraseña</h3>
        <form class="formPostForm" name="passwordForm" action="/bulletin_board/servlets/UserChangePassword" method="POST" id="passwordChangeForm">
            <ul class="ulForm">
                <li class="liForm">
                    <label for="contrasenaAnterior">Contraseña Anterior:</label> 
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="password" name="oldPassword" id="oldPassword" autofocus>
                </li>
                <li class="liForm">
                    <label for="nuevaContrasena">Nueva Contraseña:</label>
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="password" name="newPassword" id="newPassword">
                </li>
                <li class="liForm">
                    <label for="confirmarNuevaContrasena">Confirmar Nueva Contraseña:</label>
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="password" name="confirmNewPassword" id="confirmNewPassword">
                </li>
                <li class="liFormButton">  
                    <button class="formButton corporateButton" type="submit" name="" value="cambiarContraseña" id="changePassword">Cambiar Contraseña</button>
                </li>
            </ul>
        </form>
        <div class="thinLine"></div>
        <h3 class="cambiarDireccion">Cambiar Dirección Electrónica</h3>
        <form class="formPostForm" name="emailForm" action="/bulletin_board/servlets/UserChangeEmail" method="POST" id="emailChangeForm">
            <ul class="ulForm">
                <li class="liForm">
                    <label for="direccionElectronicaAnterior">Dirección Electrónica Anterior:</label> 
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="email" name="oldEmail" id="oldEmail">
                </li>
                <li class="liForm">
                    <label for="nuevaDireccionElectronica">Nueva Dirección Electrónica:</label>
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="email" name="newEmail" id="newEmail">
                </li>
                <li class="liForm">
                    <label for="confirmarNuevaContrasena">Confirmar Nueva Dirección Electrónica:</label>
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="email" name="confirmNewEmail" id="confirmNewEmail">
                </li>
                <li class="liFormButton">  
                    <button class="formButton corporateButton" type="submit" name="" value="cambiarDireccionElectronica" id="changeEmail">Cambiar Dirección Electrónica</button>
                </li>
            </ul>
        </form>
    </div>
</body>
</html>