$(document).ready(function() {

    //Validation of the form to change the details of the users.
     
    $("#passwordChangeForm").validate({
        rules : {
            oldPassword : {
            required : true,
          },
            newPassword : {
            required : true,
            maxlength : 20,
            minlength : 8,
          },	
            confirmNewPassword : {
            equalTo: "#newPassword",
            required : true,
            maxlength : 20,
            minlength : 8
          }
        },
    
        messages : {
            oldPassword :"Se necesita tu contraseña anterior.",
            newPassword :{ 
               required:"Se necesita tu nueva contraseña.",
               maxlength:"Su nueva contraseña no puede superar los 20 caracteres.",
               minlength:"Su nueva contraseña debe tener al menos 8 caracteres."
            },
            confirmNewPassword :{
               required:"Se necesita confirmar tu nueva contraseña.",
               maxlength:"Su nueva contraseña no puede superar los 20 caracteres.",
               minlength:"Su nueva contraseña debe tener al menos 8 caracteres.",
               equalTo:"La contraseña no es la misma."
            }, 
        }
    
       });

       $('#changePassword').click(function() {
        $("#passwordChangeForm").valid();
    });

       $("#emailChangeForm").validate({
        rules : {
            oldEmail : {
            required : true,
            email : true 
          },
            newEmail : {
            required : true,
            email : true 
          },	
            confirmNewEmail : {
            equalTo: "#newEmail",
            required : true,
            email : true,
          }
        },
    
        messages : {
            oldEmail :{
                required:"Se necesita tu dirección anterior.",
                email:"Esta dirección no es válida." 
            }, 
            newEmail :{
                required:"Se necesita tu nueva dirección.",
                email:"Esta nueva dirección no es válida."
            }, 
            confirmNewEmail :{
                equalTo:"Esta dirección no es la misma.",
                required:"Se necesita confirmar tu nueva dirección.",
                email:"Esta nueva dirección no es válida."
            }, 
        }
    
       });

       $('#changeEmail').click(function() {
        $("#emailChangeForm").valid();
      });

       $("#changePassword").click(function(){
        var oldPassword = $("#oldPassword").val();

        var newPassword = $("#newPassword").val();

        var confirmNewPassword = $("#confirmNewPassword").val();

  });

      $("#changeEmail").click(function(){
        var oldEmail = $("#oldEmail").val();

        var newEmail = $("#newEmail").val();

        var confirmNewEmail = $("#confirmNewEmail").val();

  }); 



    });