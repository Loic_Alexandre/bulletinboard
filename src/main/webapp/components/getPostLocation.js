//Geolocalización para Posts
$("#locationPost").change(function () {
    obtenerCoordenadas();
});

//Función que transforma direcciones de los posts en coordenadas
function obtenerCoordenadas() {
    var geocoder = new google.maps.Geocoder();
    var inputLocation = $("#locationPost").val();
    geocoder.geocode({
        "address": inputLocation
    } , function (results, status) {

        if (status == google.maps.GeocoderStatus.OK) {
             // Latitud y Longitud Obtenidas por método Geocoder(), pasadas a variables
             var latitud = results[0].geometry.location.lat();
             var longitud = results[0].geometry.location.lng();
             alert ("Ubicación actual:" + latitud + "y" + longitud);

             
    
        } else{
            alert("¡Error! Este navegador no soporta la Geolocalización.");
        }
 
});

}