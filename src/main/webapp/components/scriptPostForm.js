	
$(document).ready(function() {

//Validation of the form
 

$("#mainForm").validate({
    rules : {
      titlePost : {
        required : true,
        maxlength : 80
      },
      contentPost : {
        required : true,
        minlength : 5
      },	
      phonePost : {
        required : true,
      },
      emailPost : {
        required : true,
        email : true     
      },
      locationPost : {
        required : true,
      }
    },
    
    messages : {
      titlePost :{
        required:"Se necesita un título.",
        maxlength:"El el título no puede tener más de 80 caracteres."
      }, 
      contentPost :{
        required:"Se necesita el contenido del post",
        minlength:"El el Contenido no puede tener menos de 5 caracteres."
      }, 
      phonePost :{
        required:"Se necesita un teléfono.",
        digits:"Solo se aceptan números."
      }, 
      emailPost :{
        required:"Se necesita un email.",
        email:"Este formato de correo electrónico no es válido."
      }, 
      locationPost : "Se necesita una dirección."
    },

   });

   $('#createPost').click(function() {
    $("#mainForm").valid();
});

});