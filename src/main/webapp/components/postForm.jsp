<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Formulario de Posts</title>
        <script src="https://kit.fontawesome.com/82342f99b0.js" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.min.js"></script>
        <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"></script>
        <script type="text/javascript" src="scriptPostForm.js"></script>
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="https://use.typekit.net/ifd2oig.css">
        <link rel="stylesheet" type="text/css" href="postForm.css">
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
    </head>
</head>
<body>
    <div class="containerPost">
        <form class="formPostForm" name="postForm" action="com.bulletinboard.Servlet.NewPost" method="post" id="mainForm">
            <ul class="ulForm">
                <li class="liForm">
                    <label for="titulo">Titulo:</label> 
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="text" name="titlePost" id="titlePost" autofocus>
                </li>
                <li class="liForm">
                    <label for="contenido">Contenido:</label>
                </li>
                <li class="liFormInput">
                    <textarea class="textareaForm" id="content" name="contentPost" placeholder="" ></textarea>
                </li>
                <li class="liForm">
                    <label for="imagen">Imagen:</label> 
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="file" name="imagePost" id="imagePost">
                </li>
                <li class="liForm">
                    <label for="telefono">Telefono:</label>
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="tel" name="phonePost" id="phonePost">
                </li>
                <li class="liForm">
                    <label for="cElectronico">Correo Electronico:</label> 
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="email" name="emailPost" id="emailPost">
                </li>
                <li class="liForm"> 
                    <label for="localizacion">Localizacion:</label>
                </li>
                <li class="liFormInput">
                    <input class="inputForm" type="text" name="locationPost" id="locationPost">
                </li>
                <li class="liForm"> 
                    <label for="localizacion">Producto:</label>
               </li>
               <li class="liFormInput">
               
                </li>
                <li class="liFormButton">  
                    <button class="formButton corporateButton" type="submit" name="createPost" value="Crear Post" id="createPost">Crear Post</button>
                </li>
            </ul>
        </form>
    </div>
</body>
</html>