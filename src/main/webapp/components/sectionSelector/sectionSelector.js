$(document).ready(function () {

    let selectedSection = $(".sectionTitleSelected").html();

    console.log(selectedSection);

    $(".sectionTitle").on("click", function (e) {
        $(".sectionTitle").removeClass('sectionTitleSelected')
        $(this).addClass('sectionTitleSelected')
        selectedSection = $(this).html()
        let sectionValue;
        switch (selectedSection) {
            case "Demandas":
                sectionValue = 2;
                break;
            case "Ofertas":
                sectionValue = 1;

                break;
            case "Varios":
                sectionValue = 3;

                break;
            default:
                break;
        }

        $('#inputSection').attr('value',sectionValue)
        $('#inputSectionDesktop').attr('value',sectionValue)
        console.log(sectionValue)
    })


})