<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl7plPMrwbyC0q5dYmlOWOFXafWbLS7Pw"></script>
    <div class="searchContainer">
        <form class="mobile searchForm " >
            <input id="searchTitle" class="searchInput" type="text" placeholder="¿Qué Buscas?" value="" name="searchContent">
            <div class="distanceSection">
                <div class="distanceInputSection">
                    <input id="searchLocation" class="adressInput" type="text" placeholder="¿Dónde estás?: C/ Falsa 123" value="" >
                    <div class="searchRadiusSection">
                        <span>Buscar a </span>
                        <input id="searchDistance" class="distanceInput" type="number" min="0" placeholder="1" value="1" name="searchDistance">
                        <span>Km de distancia</span>
                    </div>
                    <input id="inputLatitud" hidden type="text" name="latitud">
                    <input id="inputLongitud" hidden type="text" name="longitud">
                    <input id="inputSection" hidden type="number" value="2" name="searchSection">
                    <input id="searchButton" class="corporateButton searchButton" type="button" value="Buscar" >
                </div>
            </div>
        </form>

        <form class="tabletDesktop searchForm ">
            <div>
                <input id="searchTitleDesktop" class="searchInput" type="text" placeholder="¿Qué Buscas?" value="" name="searchContent">
                <input id="searchLocationDesktop" class="adressInput" type="text" placeholder="¿Donde estás?: C/ Falsa 123" value="">
            </div>
            <div class="searchRadiusSection">
                <span>Buscar a </span>
                <input id="searchDistanceDesktop" class="distanceInput" type="number" min="0" placeholder="1" value="1" name="searchDistance">
                <span>Km de distancia</span>
            </div>
            <input id="inputLatitudDesktop" hidden type="text" name="latitud">
            <input id="inputLongitudDesktop" hidden type="text" name="longitud">
            <input id="inputSectionDesktop" hidden type="text" value="2" name="searchSection">
            <input id="searchButtonDesktop" class="corporateButton searchButton" type="button" value="Buscar">
        </form>
    </div>