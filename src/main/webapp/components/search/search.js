$(document).ready(function(){
    
    //Geolocalización para Posts
    $("#searchLocation").change(function () {
        obtenerCoordenadas();
    });

    //Función que transforma direcciones de los posts en coordenadas
    function obtenerCoordenadas() {
        var geocoder = new google.maps.Geocoder();
        var inputLocation = $("#searchLocation").val();
        geocoder.geocode({
            "address": inputLocation
        }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                // Latitud y Longitud Obtenidas por método Geocoder(), pasadas a variables
                var latitud = results[0].geometry.location.lat();
                var longitud = results[0].geometry.location.lng();
                $("#inputLatitud").val(latitud);
                $("#inputLongitud").val(longitud);

            } else {
                alert("¡Error! Este navegador no soporta la Geolocalización.");
            }

        });

    }

    $("#searchLocationDesktop").change(function () {
        obtenerCoordenadasDesktop();
    });

    //Función que transforma direcciones de los posts en coordenadas
    function obtenerCoordenadasDesktop() {
        var geocoder = new google.maps.Geocoder();
        var inputLocation = $("#searchLocationDesktop").val();
        geocoder.geocode({
            "address": inputLocation
        }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                // Latitud y Longitud Obtenidas por método Geocoder(), pasadas a variables
                var latitud = results[0].geometry.location.lat();
                var longitud = results[0].geometry.location.lng();
                $("#inputLatitudDesktop").val(latitud);
                $("#inputLongitudDesktop").val(longitud);

            } else {
                alert("¡Error! Este navegador no soporta la Geolocalización.");
            }

        });

    }
    
})