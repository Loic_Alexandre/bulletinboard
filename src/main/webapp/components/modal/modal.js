$(document).ready(function(){

    selectedSection = "loggIn"

    $(".closeModal").on('click', function(){
        $(".modalContainer").css('display', 'none');
    })

    $(".modalContainer").on('click', function(){
        $(".modalContainer").css('display', 'none');
    })

    $(".modalContainer .modalContentContainer").on('click', function(e){
        e.stopPropagation();
    })

    $(".loggInSectionButton").on('click', function(){
        console.log(selectedSection)
        if(selectedSection =="signIn"){
            selectedSection = "loggIn";
            console.log(selectedSection)
            $(".loggInFormContainer").removeClass("hidden");
            $(".signInFormContainer").addClass("hidden");
            $(".loggInSectionButton").addClass("sectionSelected")
            $(".signInSectionButton").removeClass("sectionSelected");
        }
    })

    $(".signInSectionButton").on('click', function(){
        console.log(selectedSection)
        if(selectedSection=="loggIn"){
            selectedSection = "signIn";
            console.log(selectedSection)
            $(".signInFormContainer").removeClass("hidden")
            $(".loggInFormContainer").addClass("hidden");
            $(".loggInSectionButton").removeClass("sectionSelected")
            $(".signInSectionButton").addClass("sectionSelected");
        }

    })
})