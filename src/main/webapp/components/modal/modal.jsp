<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<div class="modalContainer hidden">
    <div class="modalContentContainer">
        <div class="closeModal">X</div>
        <div class="loggInSignInTitleContainer">
            <h3 class="loggInSectionButton sectionHovered sectionSelected">Iniciar Sesión</h3>
            <h3>/</h3>
            <h3 class=" signInSectionButton sectionHovered">Regístrate</h3>
        </div>
        <div class="loggInFormContainer ">
            <form class="formContainer" name="userLogin" action="/bulletin_board/servlets/UserLogin" method="post">
                <span class="formText">Usuario</span>
                <input class="formInput" type="text" name="userAlias">
                <span class="formText">Contraseña</span>
                <input class="formInput" type="password" name="userPassword" >
                <input class="submitButton" type="submit" name="" value="Iniciar Sesion">
            </form>
        </div>

        <div class="signInFormContainer hidden">
            <form class="formContainer" name="userRegister" action="/bulletin_board/servlets/UserRegister" method="post">
                <span class="formText">Usuario</span><input class="formInput" type="text" name="userAlias">
                <span class="formText">Contraseña</span><input class="formInput" type="password" name="userPassword">
                <span class="formText">Teléfono</span><input class="formInput" type="number" name="numMobile">
                <span class="formText">Correo Electrónico</span><input class="formInput" type="text" name="userEmail">
                <input class="submitButton" type="submit" name="" value="Registrate">
            </form>
        </div>


    </div>
</div>