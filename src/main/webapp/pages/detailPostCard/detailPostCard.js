$(document).ready(function () {
    $("#createPostButton").on('click', function () {
        $(location).attr('href', "http://localhost:8080/bulletin_board/pages/postForm/postForm.jsp");
    })

    function loadDetailPost() {
        const valores = window.location.search;
        const urlParams = new URLSearchParams(valores);
        const postId = urlParams.get('id');
        const postIdType = typeof (postId)

        let formData = new FormData();
        formData.append("id", postId);

        fetch('http://localhost:8080/bulletin_board/servlets/PostList', {
            method: 'POST',
            body: formData
        }).
            then(function (response) {
                return response.json();
            }).then(function (postDetail) {
                createDetailPost(postDetail);
                

            });

    }


    function createDetailPost(detail){
        document.getElementById("2").innerHTML = "";
        const detailPost = `
                                <div class="detailPost">
                                    <p class="detailPostCreationDate">Creado: ${detail.creationDate}</p>
                                    <h1 class="detailPostTitle">${detail.title}</h1>
                                    <img class="imgDetailPost" id="logo" src="../../img/placeholder${Math.floor(Math.random()*5+1)}.png" alt="logo" />
                                    <p class="detailPostDescription">
                                    ${detail.content}
                                    </p>
                                    <div class="detailPostContactContainer">
                                        <div class="detailPostContact"><i class="fas fa-envelope-square detailContactIcon"></i> <span class="detailPostContactSpan">${detail.email}</span></div>
                                        <div class="detailPostContact"><i class="fas fa-phone-square detailContactIcon"></i><span class="detailPostContactSpan">${detail.phone}</span></div>
                        
                                    </div>
                                    </div>
                             `;

                document.getElementById("2").innerHTML += detailPost;
    }


    loadDetailPost();
})