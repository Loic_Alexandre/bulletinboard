<html lang="en">

<head>
    <%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
        <meta charset="UTF-8">
        <script src="https://kit.fontawesome.com/82342f99b0.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="stylesheet" href="https://use.typekit.net/ifd2oig.css">
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <link rel="stylesheet" href="../../style.css">
        <link rel="stylesheet" href="./userPage.css">
        <link rel="stylesheet" href="../postForm/postForm.css">
        <link rel="stylesheet" href="../../components/header/header.css">
        <link rel="stylesheet" href="../../components/modal/modal.css">
        <script src="./userPage.js"></script>
        <script type="text/javascript" src="../postForm/postForm.js"></script>
        <script src="../../components/header/header.js"></script>
        <script src="../../components/modal/modal.js"></script>
        <title>Creción de Post | Bulletin Board</title>

</head>
<header class="headerNavContainer">
    <%@include file="../../components/header/header.jsp" %>
        <%@include file="../../components/modal/modal.jsp" %>
</header>

<body>
    <div class="userPageMainContainer">

        <span class="userPageMainTitle">Ajustes de Usuario</span>
        <form class="userPageForm">
            <p id="userNewPasswordTitle" class="userPageFormInputTitle"><i id="userNewPasswordIcon"
                    class="far fa-arrow-alt-circle-up userPageFormIcon"></i>Cambiar contraseña</p>
            <input id="userNewPasswordInput" class="userPageInput hidden" type="text"
                placeholder="inserte su nueva contraseña">
            <p id="userNewEmailTitle" class="userPageFormInputTitle"><i id="userNewEmailIcon"
                    class="far fa-arrow-alt-circle-up userPageFormIcon"></i>Cambiar email</p>
            <input id="userNewEmailInput" class="userPageInput hidden" type="text" placeholder="inserte su nuevo email">
        </form>

        <span class="userPageMainTitle">Mis Posts</span>
        <ul id="userPagePostList" class="userPagePostList" style="list-style: none;">

        </ul>

        <div id="createPostButton" class="userPageCreatePostButton">Crear Post <i class="fas fa-plus-circle userPageCreatePostPlusSign"></i></div>


    </div>
</body>

</html>