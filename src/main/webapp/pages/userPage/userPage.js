$(document).ready(function(){

    let userNewPasswordActive = false;
    let userNewEmailActive = false;


    $("#userNewPasswordTitle").on('click', function(){

        if(userNewPasswordActive){
            userNewPasswordActive = false;
            $('#userNewPasswordInput').addClass("hidden");
            $('#userNewPasswordIcon').addClass("fa-arrow-alt-circle-up")
            $('#userNewPasswordIcon').removeClass("fa-arrow-alt-circle-down")
        }else{
            userNewPasswordActive = true;
            $('#userNewPasswordInput').removeClass("hidden");
            $('#userNewPasswordIcon').removeClass("fa-arrow-alt-circle-up")
            $('#userNewPasswordIcon').addClass("fa-arrow-alt-circle-down")
        }
            
    })

    $("#userNewEmailTitle").on('click', function(){

        if(userNewEmailActive){
            userNewEmailActive = false;
            $('#userNewEmailInput').addClass("hidden");
            $('#userNewEmailIcon').addClass("fa-arrow-alt-circle-up")
            $('#userNewEmailIcon').removeClass("fa-arrow-alt-circle-down")
        }else{
            userNewEmailActive = true;
            $('#userNewEmailInput').removeClass("hidden");
            $('#userNewEmailIcon').removeClass("fa-arrow-alt-circle-up")
            $('#userNewEmailIcon').addClass("fa-arrow-alt-circle-down")
        }
            
    })


    const postCardsInfo = [{
        id:"1",
        title: "Ofrezco trabajos de fontanería blablablablablablablablablablablablablablablablablablablablablabla",
        date:"1-1-2021"
    },
    {
        id:"2",
        title: "Se busca niñera",
        date: "1-1-2021"
    },
    {
        id:"3",
        title: "Title 3",
        date: "1-1-2021"
    },
    {
        id:"4",
        title: "Title 4",
        date: "1-1-2021"
    }];

    $("#createPostButton").on('click',function(){
        console.log("create post clicked")
        $(location).attr('href',"http://localhost:8080/bulletin_board/pages/postForm/postForm.jsp");
    })

    function createUserpostList(){
        
        document.getElementById("userPagePostList").innerHTML = "";
        for(let i = 0; i<postCardsInfo.length; i++){

            const postCard = `
                         <li id="${postCardsInfo[i].id}"class="userPagePostCardContainer">
                            <a style="text-decoration:none;color:black;" href="../detailPostCard/detailPostCard.jsp">
                                <div class="userPagePostCard">
                                <div class="userPagePostCardTitleContainer">
                                    <p class="userPagePostCardTitle">${postCardsInfo[i].title}</p>
                                </div>
                                <img class="userPagePostCardImg" id="logo" src="../../img/placeholder.png" alt="logo">
                                <p class="userPagePostcreationDate">Creado: ${postCardsInfo[i].date}</p>
                                <div class="UserPagePostCardOverlay"/>
                                </div>
                                </a>
                        </li>`;

            document.getElementById("userPagePostList").innerHTML += postCard;
        }
    }

    createUserpostList();

})