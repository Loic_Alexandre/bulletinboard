<html lang="en">

<head>
    <%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
        <meta charset="UTF-8">
        <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDl7plPMrwbyC0q5dYmlOWOFXafWbLS7Pw"></script>
        <script src="https://kit.fontawesome.com/82342f99b0.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="stylesheet" href="https://use.typekit.net/ifd2oig.css">
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/jquery.validate.min.js"></script>
        <script src="https://cdn.jsdelivr.net/jquery.validation/1.16.0/additional-methods.min.js"></script>
        <link rel="stylesheet" href="../../style.css">
        <link rel="stylesheet"  href="./postForm.css">
        <link rel="stylesheet" href="../../components/header/header.css">
        <link rel="stylesheet" href="../../components/modal/modal.css">
        <script type="text/javascript" src="postForm.js"></script>
        <script src="../../components/header/header.js"></script>
        <script src="../../components/modal/modal.js"></script>
        <title>Creción de Post | Bulletin Board</title>

</head>
<header class="headerNavContainer">
    <%@include file="../../components/header/header.jsp" %>
    <%@include file="../../components/modal/modal.jsp" %>
</header>
<body>
    <div class="containerPost">
        <form class="formPostForm" name="postForm" action="/bulletin_board/servlets/NewPost" method="POST" id="mainForm">
            <label class="labelPostTitle" for="titulo">Título:</label>
            <input class="inputPostForm" type="text" name="titlePost" autofocus>
            <label class="labelPostTitle" for="contenido">Descripción:</label>
            <textarea class="textareaForm" id="content" name="contentPost" placeholder=""></textarea>
            <label class="labelPostTitle" for="localizacion">Dirección:</label>
            <input id="locationPost"  class="inputPostForm" type="text" name="locationPost">
            <input type="text" id="inputLatitud" class="form-control" placeholder="latitude" name="latitud" hidden>
            <input type="text" id="inputLongitud" class="form-control" placeholder="longitud" name="longitud" hidden>
            <label class="labelPostTitle">Categoría</label>
            <select class="inputPostForm" style="width: fit-content;" name="idSection">
                <optgroup class="postFormOptGroup">
                    <option id="demandasOption" value=""></option>
                    <option id="ofertasOption"value=""></option>
                    <option id="variosOption"value=""></option>
                </optgroup>
            </select>
            <label class="labelPostTitle" for="imagen">Imagen:</label>
            <input id="imageFake" class="imagePostInput hidden" type="text" name="imagePostFake">
            <input class="imagePostInput" type="file" name="imagePost">
            <label class="labelPostTitle" for="telefono">Teléfono:</label>
            <input class="inputPostForm" type="tel" name="phonePost">
            <label class="labelPostTitle" for="cElectronico">Correo Electrónico:</label>
            <input class="inputPostForm" type="email" name="emailPost">
            <button class="formButton corporateButton" type="submit" name="" value="Crear Post">Crear Post</button>
        </form>
    </div>
</body>

</html>