$(document).ready(function () {


    //Geolocalización para Posts
    $("#locationPost").change(function () {
        obtenerCoordenadas();
    });

    //Función que transforma direcciones de los posts en coordenadas
    function obtenerCoordenadas() {
        var geocoder = new google.maps.Geocoder();
        var inputLocation = $("#locationPost").val();
        geocoder.geocode({
            "address": inputLocation
        }, function (results, status) {

            if (status == google.maps.GeocoderStatus.OK) {
                // Latitud y Longitud Obtenidas por método Geocoder(), pasadas a variables
                var latitud = results[0].geometry.location.lat();
                var longitud = results[0].geometry.location.lng();
                $("#inputLatitud").val(latitud);
                $("#inputLongitud").val(longitud);

            } else {
                alert("¡Error! Este navegador no soporta la Geolocalización.");
            }

        });

    }


    function changeImg() {
        let imgUrl = `../../img/placeholder${Math.floor(Math.random() * 5) + 1}.png`
        $('#imageFake').attr('value', imgUrl)
        console.log(imgUrl);
    }

    function loadSections() {

        fetch('http://localhost:8080/bulletin_board/servlets/NewPost').
            then(function (arraySectionsJSON) {
                return arraySectionsJSON.json();
            }).then(function (arraySectionsParsed) {
                console.log(arraySectionsParsed)
                $('#demandasOption').text(arraySectionsParsed[1].sectionName)
                $('#demandasOption').val(arraySectionsParsed[1].idSection)

                $('#ofertasOption').text(arraySectionsParsed[0].sectionName)
                $('#ofertasOption').val(arraySectionsParsed[0].idSection)

                $('#variosOption').text(arraySectionsParsed[2].sectionName)
                $('#variosOption').val(arraySectionsParsed[2].idSection)

            });

    }

    changeImg();
    loadSections();


})