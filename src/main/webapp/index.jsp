<%@ page import="java.util.*" %>
<html>
<head>
    <%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
        <meta charset="UTF-8">
        <script src="https://kit.fontawesome.com/487bfdf744.js" crossorigin="anonymous"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
        <link href="https://fonts.googleapis.com/css2?family=Lobster&display=swap" rel="stylesheet">
        <link rel="preconnect" href="https://fonts.gstatic.com">
        <link rel="stylesheet" href="https://use.typekit.net/ifd2oig.css">
        <script src="./index.js"></script>
        <script src="./components/header/header.js"></script>
        <script src="./components/modal/modal.js"></script>
        <script src="./components/search/search.js"></script>
        <script src="./components/sectionSelector/sectionSelector.js"></script>
        <script src="./components/postCard/postCard.js"></script>
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="./components/header/header.css">
        <link rel="stylesheet" href="./components/modal/modal.css">
        <link rel="stylesheet" href="./components/search/search.css">
        <link rel="stylesheet" href="./components/sectionSelector/sectionSelector.css">
        <link rel="stylesheet" href="./components/postCard/postCard.css">
        <title>Bulletin Board</title>

</head>
<header class="headerNavContainer">
    <%@include file="./components/header/header.jsp" %>
    <%@include file="./components/sectionSelector/sectionSelector.jsp" %>
    <%@include file="./components/search/search.jsp" %>
    <%@include file="./components/modal/modal.jsp" %>
</header>

<body>
    <div class="mainContainer">
        <ul style="list-style: none;" id="1" class="listResultsContainer">
          
        </ul>

        <%
        if(session.getAttribute("usuario")!=null){
            out.println("<div id='createPostButton'class='createPostButton'>Crear Post <i class='fas fa-plus-circle createPostPlusSign'></i></div>");
        }else{
            out.println("<div id='createPostButtonLoggin'class='createPostButton'>Crear Post <i class='fas fa-plus-circle createPostPlusSign'></i></div>");
        }
        %>
    </div>
</body>

</html>